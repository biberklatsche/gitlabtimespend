#! /usr/bin/env node
const {program} = require('commander');
const conf = new (require('conf'))();
const chalk = require('chalk');
const childProcess = require('child_process');
const loading = require('loading-cli');
const loadOptions = {
    "interval": 200,
    "frames": ["|", "/", "-", "\\", "|", "/", "-", "\\"]
}

const getBranchName = async () => {
    return new Promise(function (resolve, reject) {
        const process = childProcess.spawn('git', ['symbolic-ref', '--short', 'HEAD']);
        let buffers = [];
        let errors = [];
        process.stdout.on('data', data => {
            buffers.push(data)
        });
        process.stderr.on('data', data => {
            errors.push(data);
        });
        process.on('close', function (code) {
            if (errors.lenght > 0) {
                const result = Buffer.concat(errors).toString();
                reject(result);
                return;
            }
            const result = Buffer.concat(buffers).toString();
            resolve(result.replace(/(\r\n|\n|\r)/gm, "").trim());

        });
        process.on('error', function (err) {
            reject(err);
        });
    });

}

const getIssue = async (domain, token, issueId, branchNameParts) => {
    return new Promise(async function (resolve, reject) {
        let result = null;
        let index = 0;
        while (result == null && index < branchNameParts.length) {
            const encodedName = encodeURI(branchNameParts[index]);
            result = await new Promise(function (resolve, reject) {
                const request = `curl -s --header \"PRIVATE-TOKEN: ${token}\" \"https://${domain}/api/v4/search?scope=issues&search=${encodedName}\"`;
                // console.log('Search issue request: ', request);
                childProcess.exec(request,
                    (error, stdout, stderr) => {
                        if (error) {
                            reject(error);
                            return;
                        }
                        if (stderr) {
                            reject(stderr);
                            return;
                        }
                        const result = stdout.trim();
                        if (!result.startsWith('[') || !result.endsWith(']')) {
                            resolve(null);
                            return;
                        }
                        const ps = JSON.parse(result);
                        if (ps.length === 0) {
                            resolve(null);
                            return;
                        }
                        if (ps.length > 1) {
                            const p = ps.find(p => p.iid == issueId)
                            resolve(p);
                            return;
                        }
                        resolve(ps[0]);
                    }
                );
            }).catch(e => reject(e));
            index++;
        }
        if (result === null) {
            reject('Could not find issue.')
        }
        resolve(result);
    });

}

const book = (domain, token, projectId, issueId, time) => {
    return new Promise(function (resolve, reject) {
        childProcess.exec(`curl -s --request POST --header \"PRIVATE-TOKEN: ${token}\" "https://${domain}/api/v4/projects/${projectId}/issues/${issueId}/add_spent_time?duration=${time}"`,
            (error, stdout, stderr) => {
                if (error) {
                    reject(error);
                    return;
                }
                if (stderr) {
                    reject(stderr);
                    return;
                }
                const result = stdout.trim();
                if (!result.includes('total_time_spent')) {
                    reject(`Could not book the time because: ${result}`);
                    return;
                }
                resolve();
            }
        );
    });
}

program
    .command('token <gitlabToken>')
    .description('Sets the personal gitlab token.')
    .action((gitlabToken) => {
        conf.set('token', gitlabToken);
    });


program.command('domain <gitlabDomain>')
    .description('Sets the gitlab domain.')
    .action((gitlabDomain) => {
        conf.set('domain', gitlabDomain);
    });


program.command('info')
    .description('Prints out your current token and the gitlab domain.')
    .action(() => {
        const token = conf.get('token');
        const domain = conf.get('domain');
        console.log(chalk.gray(`Domain: `) + chalk.white(`${domain}`));
        console.log(chalk.gray(`Token:  `) + chalk.white(`${token}`));
    });

program.argument('<time>', 'Time to spend, for example <1h> or <1h30m>.')
    .option('-r, --remove', 'removes this time')
    .action(async (time, options) => {
        const token = conf.get('token');
        const domain = conf.get('domain');
        if (!token) {
            console.log(chalk.red('You have to set a token with the command:'));
            console.log(chalk.white('   spend token <your-token>'));
        }
        if (!domain) {
            console.log(chalk.red('You have to set the gitlab domain with the command:'));
            console.log(chalk.white('   spend domain <your-gitlab-domain>'));
        }
        if (!domain || !token) {
            return;
        }

        let load = loading(loadOptions).start('Determine branch name...');
        const branchName = await getBranchName().catch(err => {
            load.fail(chalk.red(`  error: ${err}`));
        });
        if (!branchName) {
            return;
        }
        load.succeed(chalk.gray(`branch:  ${branchName}`));
        const branchNameParts = branchName.split('-');
        const issueId = branchNameParts.shift();
        if (isNaN(issueId)) {
            console.log(chalk.red(`'${branchName}' is not a gitlab feature branch.`));
            return;
        }
        load = loading(loadOptions).start(`Determine issue...`);
        const issue = await getIssue(domain, token, issueId, branchNameParts).catch(err => {
            load.fail(chalk.red(`  error: ${err}`));
        });
        if (!issue) {
            return;
        }
        load.succeed(chalk.gray(`issue: [${issue.project_id}] ${issue.title}`));
        load = loading(loadOptions).start(`Book time...`);
        if(options.remove) {
            time = '-' + time;
        }
        await book(domain, token, issue.project_id, issueId, time).catch(err => {
            load.fail(chalk.red(`  error: ${err}`));
        });
        load.succeed(chalk.green.bold(`${time} booked`));
    });
program.parse(process.argv);
